from rest_framework import authentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import *
from rest_framework.generics import *
from apps.users.user_premissions import *
from . serializers import *
from rest_framework.status import *
from . models import *


class CreateCompany(CreateAPIView):
    permission_classes = (GeneralUserPermissions,)
    # authentication_classes = (authentication.BaseAuthentication,)
    serializer_class = CompanySerializer
    queryset = Company.objects.all()

    def create(self, request, *args, **kwargs):
        name = request.data.get('business_name', None)
        business_type = request.data.get('business_type', None)
        address = request.data.get('address', None)
        phone_number = request.data.get('phone_number', None)
        regis_number = request.data.get('registration_number', None)
        tin_number = request.data.get('tin_number', None)
        # user_id = request.user.id

        if request.user.companies.all():
            return Response({'detail': 'This user already has a company', 'response': 'Failure'},
                            status=HTTP_400_BAD_REQUEST)

        else:
            company = Company.objects.create(
                business_name=name,
                business_type=business_type,
                address=address,
                phone_number=phone_number,
                registration_number=regis_number,
                tin_number=tin_number,
                is_active=True,
                created_by=request.user,
            )
            serializer = CompanySerializer(company, context={'request': request})
            return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_201_CREATED)


class CompanyList(ListAPIView):
    permission_classes = (GeneralUserPermissions,)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


# class CompanyDetail(RetrieveAPIView):
#     permission_classes = (GeneralUserPermissions,)
#     serializer_class = CompanySerializer
#     lookup_field = 'id'
#
#     def get_queryset(self):
#         return Company.objects.filter(created_by=self.request.user)


class CompanyDetail(APIView):
    permission_classes = (GeneralUserPermissions,)

    def get(self, request):
        if request.user.companies.all():
            queryset = Company.objects.filter(created_by=request.user)
            serializer = CompanySerializer(queryset, context={'request': request}, many=True)
            return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_200_OK)
        else:
            return Response({'detail': 'User has no company setup', 'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)