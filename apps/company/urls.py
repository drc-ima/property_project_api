from django.urls import path
from . views import *

app_name = 'company'

urlpatterns = [
    path('create/', CreateCompany.as_view(), name='create'),
    path('list/', CompanyList.as_view(), name='list'),
    path('detail/', CompanyDetail.as_view(), name='detail'),
    # path('annex/detail/', CompanyDetailAnnex.as_view(), name='annex-detail'),
]