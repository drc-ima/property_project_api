from rest_framework.serializers import *
from .models import *


class CompanySerializer(ModelSerializer):
    created_by = ReadOnlyField(source='created_by.get_full_name')

    class Meta:
        model = Company
        fields = ('id', 'business_type', 'business_name', 'business_logo', 'address',
                  'phone_number', 'registration_number', 'tin_number', 'is_active', 'created_at', 'created_by')
        read_only_fields = ('id', 'created_at', 'created_by')
