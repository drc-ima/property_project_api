# Generated by Django 2.1.7 on 2019-03-15 12:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0003_auto_20190304_0955'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='business_type',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
