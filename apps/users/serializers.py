from django.contrib.auth import authenticate
from rest_framework.serializers import *
from django.conf import settings
from .models import *


class LoginSerializer(Serializer):
    email = CharField()
    password = CharField()

    def validate(self, attrs):
        user = authenticate(username=attrs['email'], password=attrs['password'])

        if not user:
            raise ValidationError('Incorrect email or password.')
        if not user.is_active:
            raise ValidationError('User is disabled.')
        return {'user': user}

class UserSerializer(ModelSerializer):
    full_name = URLField(source='get_full_name')
    sex_display = SerializerMethodField()
    user_type_display = SerializerMethodField()

    class Meta:
        model = User
        fields = (
            'id', 'username', 'email', 'first_name', 'last_name', 'sex', 'sex_display', 'date_of_birth',
            'is_active', 'user_type', 'user_type_display', 'user_profile', 'date_joined', 'last_login', 'full_name'
        )
        read_only_fields = ('id', 'date_joined')

    def get_sex_display(self, obj):
        return obj.get_sex_display()

    def get_user_type_display(self, obj):
        return obj.get_user_type_display()


class LogoutSerializer(ModelSerializer):
    pass
