import json
import os

import requests
from django.contrib.auth import authenticate, login, logout
from rest_framework import authentication
from rest_framework.generics import *
from rest_framework.views import *
from rest_framework.status import *
from apps.users.user_premissions import GeneralUserPermissions
from rest_framework.permissions import *
from rest_framework.response import Response
from .serializers import *


class UserList(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer
    queryset = User.objects.all()


class CsrfExemptSessionAuthentication(authentication.SessionAuthentication):
    def enforce_csrf(self, request):
        return


class LoginView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return Response({'results': UserSerializer(user).data, 'response': 'Success'}, status=HTTP_200_OK)


class UserLogout(APIView):
    permission_classes = (GeneralUserPermissions,)

    def post(self, request):
        logout(request)
        return Response({'results': 'User Logged out', 'response': 'Success'}, status=HTTP_200_OK)


class UserLogin(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        email = request.data.get('email')
        password = request.data.get('password')

        if not email:
            return Response({'detail': 'Please email is required', 'response': 'Failure1'}, status=HTTP_400_BAD_REQUEST)

        if not password:
            return Response({'detail': 'Please password is required', 'response': 'Failure2'},
                            status=HTTP_400_BAD_REQUEST)

        try:
            user = authenticate(username=email, password=password)
            if user is not None and user.is_active:
                login(request, user)
                url = f'http://{request.get_host()}/auth/token/'
                payload = {
                    'username': email,
                    'password': password,
                    'grant_type': 'password',
                }

                headers = {'content-type': 'application/x-www-form-urlencoded',
                           'Authorization': f'JWT {os.environ.get("BASE_64_MASHUP")}'}
                myrequests = requests.post(url, payload, headers=headers)
                myuser = User.objects.get(username=email)
                myuser.token = json.loads(myrequests.text)
                myuser.save()
                return Response({'results': {
                    'token': json.loads(myrequests.text),
                    'id': user.id,
                    'full_name': user.get_full_name(),
                    'username': user.username,
                    # 'user_profile': user.user_profile,
                    'date_of_birth': user.date_of_birth,
                    'user_type': user.get_user_type_display(),
                    'phone_number': user.phone_number,
                    'date_joined': user.date_joined,

                }, 'response': 'success'}, status=HTTP_200_OK)
                # myuser = User.objects.get(username=email)
                # myuser.token = json.loads(myrequests.text)
                # myuser.save()
            else:
                return Response({'detail': 'Credential do not match or user is not active', 'response': 'Failure3'},
                                status=HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            return Response({'detail': 'Email does not exist', 'response': 'Failure4'}, status=HTTP_400_BAD_REQUEST)


class UserLogoutAnnex(APIView):
    permission_classes = (GeneralUserPermissions,)

    def post(self, request):
        user = User.objects.get(username=request.user.username)
        user.token = ''
        user.save()
        return Response({'results': 'User Logged out'}, status=HTTP_200_OK)


class UserSignUp(APIView):
    permission_classes = (AllowAny, )

    def post(self, request):
        email = request.data.get('email', None)
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        phone_number = request.data.get('phone_number', None)
        password = request.data.get('password', None)

        try:
            my_user = User.objects.create(
                email=email,
                username=email,
                phone_number=phone_number,
                first_name=first_name,
                last_name=last_name,
                user_type='MOU',
            )
            my_user.set_password(password)
            my_user.save()
            user = UserSerializer(my_user, context={'request': request})
            return Response({'results': user.data, 'response': 'Success'}, status=HTTP_201_CREATED)
        except ValueError:
            return Response({'details': 'Email already exist', 'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)


class SignUp(APIView):
    permission_classes = (AllowAny, )

    def post(self, request):
        email = request.data.get('email', None)
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        password = request.data.get('password', None)
        user_profile = request.data.get('user_profile', None)

        if not email:
            return Response({'detail': 'Email is required', 'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)
        if not password:
            return Response({'detail': 'Password is required', 'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)
        myuser = User.objects.create(
            email=email,
            username=email,
            first_name=first_name,
            last_name=last_name,
            user_type='MOU',
            user_profile=user_profile,
        )
        myuser.set_password(password)
        myuser.save()
        my_user = UserSerializer(myuser, context={'request': request})
        return Response({'results': my_user.data, 'response': 'Success'}, status=HTTP_200_OK)


class UserDetail(RetrieveAPIView):
    permission_classes = (GeneralUserPermissions,)
    serializer_class = UserSerializer
    lookup_field = 'id'
    queryset = User.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class UpdateProfile(APIView):
    permission_classes = (GeneralUserPermissions, )

    def post(self, request):
        user = request.data.get('id')
        profile = request.data

        try:
            serializer = UserSerializer(user, context={'request': request}, data=profile, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_200_OK)
        except:
            serializer = UserSerializer(user, context={'request': request}, data=profile, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=HTTP_200_OK)
            serializer = UserSerializer(profile, context={'request': request})
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
        finally:
            serializer = UserSerializer(user, context={'request': request})
            return Response(serializer.data, status=HTTP_200_OK)
