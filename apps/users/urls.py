from django.urls import path
from .views import *

app_name = 'users'

urlpatterns = [
    path('login/', UserLogin.as_view(), name='login'),
    path('annex/login/', LoginView.as_view(), name='annex-login'),
    path('login/', UserLogout.as_view(), name='login'),
    path('annex/logout/', UserLogoutAnnex.as_view(), name='annex-logout'),
    path('signup/', SignUp.as_view(), name='signup'),
    path('detail/<id>/', UserDetail.as_view(), name='detail'),
    path('list/', UserList.as_view()),
    # path('update/', UpdateProfile.as_view()),
]