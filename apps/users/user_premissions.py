from rest_framework.permissions import BasePermission
from django.contrib.auth.models import AnonymousUser


class GeneralUserPermissions(BasePermission):
    """
    Allows access to all user types except Anonymous user
    """

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False

        if request.user.user_type == 'MOU' and request.user.is_authenticated:
            return True
        else:
            return False


class HasTokenUser(BasePermission):
    """
    Allows access to all user types with Token except Anonymous user
    """

    def has_permission(self, request, view):
        if request.user == AnonymousUser():
            return False
        if request.user.user_type == 'MOU' and request.user.token is not None:
            return True
        else:
            return False


