from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, username, first_name, last_name, password=None):
        if not username:
            raise ValueError('Username is required')

        user = self.model(
            username=username,
            first_name=first_name,
            last_name=last_name,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, first_name, last_name, password):
        user = self.create_user(username=username, first_name=first_name, last_name=last_name, password=password)
        user.is_superuser = True
        user.is_active = True
        user.is_staff = True
        user.save(using=self._db)
        return user


USER_TYPE = {
    ('ADU', 'Admin User'),
    ('MOU', 'Mobile User')
}

SEX = {
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other')
}


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(_('Username'), unique=True, max_length=255)
    email = models.EmailField(_('Email Address'), blank=True, null=False)
    first_name = models.CharField(_('First Name'), max_length=255, blank=True, null=True)
    last_name = models.CharField(_('Last Name'), max_length=255, blank=True, null=True)
    is_active = models.BooleanField(_('Action Status'), default=True)
    is_superuser = models.BooleanField(_('Superuser Status'), default=False)
    is_staff = models.BooleanField(_('Staff Status'), default=False)
    user_profile = models.FileField(_('User Profile'), upload_to='media/', blank=True, null=True)
    phone_number = models.CharField(_('Phone Number'), max_length=255, blank=True, null=True)
    date_of_birth = models.DateField(_('Date of Birth'), blank=True, null=True)
    user_type = models.CharField(_('User Type'), max_length=255, choices=USER_TYPE, blank=True, null=True)
    sex = models.CharField(_('Sex'), max_length=255, choices=SEX, blank=True, null=True)
    date_joined = models.DateTimeField(_('Date Joined'), default=timezone.now)
    token = models.CharField(_('Token'), max_length=500, blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def get_short_name(self):
        return self.first_name

    def get_dob(self):
        return self.date_of_birth

    class Meta:
        verbose_name_plural = 'Users'
        verbose_name = 'user'
        ordering = ('id', 'date_joined')

    def __str__(self):
        return f'{self.get_full_name()}'
