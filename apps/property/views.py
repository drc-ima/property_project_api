from django.db.models import Q
from rest_framework.views import *
from rest_framework.generics import *
from apps.users.user_premissions import *
from . serializers import *
from rest_framework.status import *
from rest_framework.permissions import *
from .models import *
from decimal import *


class ListProperties(ListAPIView):
    permission_classes = (AllowAny,)
    queryset = Property.objects.filter(is_active=True).order_by('created_at')
    serializer_class = PropertySerializer


class ListPropertyForSale(ListAPIView):
    permission_classes = (AllowAny,)
    queryset = Property.objects.filter(is_active=True, is_for_sale=True).order_by('created_at')
    serializer_class = PropertySerializer


class ListPropertiesForRent(ListAPIView):
    permission_classes = (AllowAny,)
    queryset = Property.objects.filter(is_active=True, is_for_rent=True).order_by('created_at')
    serializer_class = PropertySerializer


class SearchProperty(APIView):
    """
    Search for property by type, location, title
    """
    permission_classes = (AllowAny, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            results = Property.objects.filter(is_active=True)
            for term in search.split():
                results = results.filter(Q(title__contains=term)
                                         | Q(property_type__contains=term)
                                         | Q(location__contains=term)
                                         | Q(landmark__contains=term))

            if not results:
                return Response({'detail': 'Your search was not found', 'response': 'Failure'},
                                status=HTTP_400_BAD_REQUEST)
            serializer = PropertySerializer(results, context={'request': request}, many=True)
            return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_200_OK)
        return Response({'detail': 'Search Text is empty', 'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)


class FilterProperties(APIView):
    """
    Filters properties by price, type, location, rent duration, rent advance, for rent, for sale, has compound,
    is furnished, is negotiable, number of rooms, number of washrooms,
    """
    permission_classes = (AllowAny, )

    def post(self, request):
        results = Property.objects.all().order_by('created_at')

        price = request.data.get('price', None)
        property_type = request.data.get('property_type', None)
        location = request.data.get('location', None)
        rent_duration = request.data.get('rent_duration', None)
        rent_advance = request.data.get('rent_advance', None)
        for_rent = request.data.get('for_rent', None)
        for_sale = request.data.get('for_sale', None)
        is_compound = request.data.get('is_compound', None)
        is_furnished = request.data.get('is_furnished', None)
        is_negotiable = request.data.get('is_negotiable', None)
        bedrooms = request.data.get('bedrooms', None)
        washroom = request.data.get('washroom', None)

        if price is not None:
            results = Property.objects.filter(price=Decimal(price))

        if property_type is not None:
            results = Property.objects.filter(property_type__exact=property_type)

        if location is not None:
            results = Property.objects.filter(location__exact=location)

        if rent_duration is not None:
            results = Property.objects.filter(rent_duration=rent_duration)

        if rent_advance is not None:
            results = Property.objects.filter(rent_advance=rent_advance)

        if for_sale is not None:
            results = Property.objects.filter(is_for_sale=for_sale)

        if for_rent is not None:
            results = Property.objects.filter(is_for_rent=for_rent)

        if is_compound is not None:
            results = Property.objects.filter(is_compound=is_compound)

        if is_furnished is not None:
            results = Property.objects.filter(is_furnished=is_furnished)

        if is_negotiable is not None:
            results = Property.objects.filter(is_negotiable=is_negotiable)

        if bedrooms is not None:
            results = Property.objects.filter(bedrooms=bedrooms)

        if washroom is not None:
            results = Property.objects.filter(washroom=washroom)

        serializer = PropertySerializer(results, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_200_OK)


class ListLands(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='land', is_active=True).order_by('created_at')


class ListLandForSale(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='land', is_for_sale=True, is_active=True).order_by('created_at')


class ListLandForRent(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='land', is_for_rent=True, is_active=True).order_by('created_at')


class ListApartments(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='apart', is_active=True).order_by('created_at')


class ListApartmentsForRent(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='apart', is_for_rent=True, is_active=True).order_by('created_at')


class ListApartmentForSale(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='apart', is_for_sale=True, is_active=True).order_by('created_at')


class ListOffices(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='off', is_active=True).order_by('created_at')


class ListOfficeForRent(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='off', is_for_rent=True, is_active=True).order_by('created_at')


class ListOfficeForSale(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='off', is_for_sale=True, is_active=True).order_by('created_at')


class ListHouses(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='hse', is_active=True).order_by('created_at')


class ListHouseForRent(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='hse', is_for_rent=True, is_active=True).order_by('created_at')


class ListHouseForSale(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='hse', is_for_sale=True, is_active=True).order_by('created_at')


class ListShops(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='shp', is_active=True).order_by('created_at')


class ListShopForRent(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='shp', is_for_rent=True, is_active=True).order_by('created_at')


class ListShopForSale(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer
    queryset = Property.objects.filter(property_type='shp', is_for_sale=True, is_active=True).order_by('created_at')


class ListCompanyProperties(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PropertySerializer

    def get_queryset(self):
        return Property.objects.filter(
            created_by__created_by=self.request.user
        ).order_by('created_at')

    def list(self, request, *args, **kwargs):
        user_id = self.request.user.id
        queryset = Property.objects.filter(created_by__created_by_id=user_id).order_by('created_at')
        serializer = PropertySerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_200_OK)


class CreateLand(APIView):
    permission_classes = (GeneralUserPermissions,)

    def post(self, request):
        location = request.data.get('location', '')
        title = request.data.get('title', '')
        description = request.data.get('description', '')
        price = request.data.get('price')
        for_sale = request.data.get('for_sale')
        for_rent = request.data.get('for_rent')
        size = request.data.get('size', '')
        unit = request.data.get('unit', '')
        landmark = request.data.get('landmark', '')
        land_type = request.data.get('land_type', '')
        rent_duration = request.data.get('rent_duration', '')
        rent_advance = request.data.get('rent_advance')
        is_negotiable = request.data.get('is_negotiable')
        photo = request.data.get('photo')

        if request.user.companies.all():
            my_property = Property.objects.create(
                location=location,
                title=title,
                description=description,
                price=price,
                is_for_rent=for_rent,
                is_for_sale=for_sale,
                size=size,
                unit=unit,
                photo=photo,
                landmark=landmark,
                land_type=land_type,
                rent_advance=rent_advance,
                rent_duration=rent_duration,
                is_negotiable=is_negotiable,
                is_active=True,
                created_by=Company.objects.get(created_by=request.user),
                property_type='land'
            )
            my_property.save()
            serializer = PropertySerializer(my_property, context={'request': request})
            return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_201_CREATED)
        else:
            return Response({'detail': 'Setup an Company profile before enlisting your properties',
                             'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)


class CreateApartment(APIView):
    permission_classes = (GeneralUserPermissions,)

    def post(self, request):
        location = request.data.get('location', '')
        title = request.data.get('title', '')
        description = request.data.get('description', '')
        price = request.data.get('price'),
        for_sale = request.data.get('for_sale')
        for_rent = request.data.get('for_rent')
        is_compound = request.data.get('is_compound')
        has_washroom = request.data.get('has_washroom')
        landmark = request.data.get('landmark', '')
        bedrooms = request.data.get('bedrooms')
        washroom = request.data.get('washroom')
        is_furnished = request.data.get('is_furnished')
        is_broker = request.data.get('is_broker')
        rent_duration = request.data.get('rent_duration', '')
        rent_advance = request.data.get('rent_advance')
        is_negotiable = request.data.get('is_negotiable')
        photo = request.data.get('photo')

        if request.user.companies.all():
            my_property = Property.objects.create(
                location=location,
                is_compound=is_compound,
                has_washroom=has_washroom,
                bedrooms=bedrooms,
                washroom=washroom,
                is_furnished=is_furnished,
                is_broker=is_broker,
                photo=photo,
                title=title,
                description=description,
                price=price,
                is_for_rent=for_rent,
                is_for_sale=for_sale,
                landmark=landmark,
                rent_advance=rent_advance,
                rent_duration=rent_duration,
                is_negotiable=is_negotiable,
                is_active=True,
                created_by=Company.objects.get(created_by=request.user),
                property_type='apart'
            )
            my_property.save()
            serializer = PropertySerializer(my_property, context={'request': request})
            return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_201_CREATED)
        else:
            return Response({'detail': 'Setup an Company profile before enlisting your properties',
                             'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)


class CreateOffice(APIView):
    permission_classes = (GeneralUserPermissions,)

    def post(self, request):
        location = request.data.get('location', '')
        title = request.data.get('title', '')
        description = request.data.get('description', '')
        price = request.data.get('price')
        for_sale = request.data.get('for_sale')
        for_rent = request.data.get('for_rent')
        is_compound = request.data.get('is_compound')
        has_washroom = request.data.get('has_washroom')
        landmark = request.data.get('landmark', '')
        bedrooms = request.data.get('offices')
        # washroom = request.data.get('washroom')
        is_furnished = request.data.get('is_furnished')
        is_broker = request.data.get('is_broker')
        rent_duration = request.data.get('rent_duration', '')
        rent_advance = request.data.get('rent_advance')
        is_negotiable = request.data.get('is_negotiable')
        photo = request.data.get('photo')

        if request.user.companies.all():
            my_property = Property.objects.create(
                location=location,
                is_compound=is_compound,
                has_washroom=has_washroom,
                bedrooms=bedrooms,
                # washroom=washroom,
                is_furnished=is_furnished,
                is_broker=is_broker,
                photo=photo,
                title=title,
                description=description,
                price=price,
                is_for_rent=for_rent,
                is_for_sale=for_sale,
                landmark=landmark,
                rent_advance=rent_advance,
                rent_duration=rent_duration,
                is_negotiable=is_negotiable,
                is_active=True,
                created_by=Company.objects.get(created_by=request.user),
                property_type='off'
            )
            my_property.save()
            serializer = PropertySerializer(my_property, context={'request': request})
            return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_201_CREATED)
        else:
            return Response({'detail': 'Setup an Company profile before enlisting your properties',
                             'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)


class CreateHouses(APIView):
    permission_classes = (GeneralUserPermissions,)

    def post(self, request):
        location = request.data.get('location', '')
        title = request.data.get('title', '')
        description = request.data.get('description', '')
        price = request.data.get('price')
        for_sale = request.data.get('for_sale')
        for_rent = request.data.get('for_rent')
        is_compound = request.data.get('is_compound')
        has_washroom = request.data.get('has_washroom')
        landmark = request.data.get('landmark', '')
        bedrooms = request.data.get('bedrooms')
        washroom = request.data.get('washroom')
        is_furnished = request.data.get('is_furnished')
        is_broker = request.data.get('is_broker')
        rent_duration = request.data.get('rent_duration', '')
        rent_advance = request.data.get('rent_advance')
        is_negotiable = request.data.get('is_negotiable')
        photo = request.data.get('photo')

        if request.user.companies.all():
            my_property = Property.objects.create(
                location=location,
                is_compound=is_compound,
                has_washroom=has_washroom,
                bedrooms=bedrooms,
                washroom=washroom,
                is_furnished=is_furnished,
                is_broker=is_broker,
                photo=photo,
                title=title,
                description=description,
                price=price,
                is_for_rent=for_rent,
                is_for_sale=for_sale,
                landmark=landmark,
                rent_advance=rent_advance,
                rent_duration=rent_duration,
                is_negotiable=is_negotiable,
                is_active=True,
                created_by=Company.objects.get(created_by=request.user),
                property_type='hse'
            )
            my_property.save()
            serializer = PropertySerializer(my_property, context={'request': request})
            return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_201_CREATED)
        else:
            return Response({'detail': 'Setup an Company profile before enlisting your properties',
                             'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)


class CreateShops(APIView):
    permission_classes = (GeneralUserPermissions,)

    def post(self, request):
        location = request.data.get('location', '')
        title = request.data.get('title', '')
        description = request.data.get('description', '')
        price = request.data.get('price')
        for_sale = request.data.get('for_sale')
        for_rent = request.data.get('for_rent')
        # is_compound = request.data.get('is_compound')
        # has_washroom = request.data.get('has_washroom')
        landmark = request.data.get('landmark', '')
        # bedrooms = request.data.get('bedrooms')
        # washroom = request.data.get('washroom')
        # is_furnished = request.data.get('is_furnished')
        is_broker = request.data.get('is_broker')
        rent_duration = request.data.get('rent_duration', '')
        rent_advance = request.data.get('rent_advance')
        is_negotiable = request.data.get('is_negotiable')
        photo = request.data.get('photo')

        if request.user.companies.all():
            my_property = Property.objects.create(
                location=location,
                # is_compound=is_compound,
                # has_washroom=has_washroom,
                # bedrooms=bedrooms,
                # washroom=washroom,
                # is_furnished=is_furnished,
                is_broker=is_broker,
                photo=photo,
                title=title,
                description=description,
                price=price,
                is_for_rent=for_rent,
                is_for_sale=for_sale,
                landmark=landmark,
                rent_advance=rent_advance,
                rent_duration=rent_duration,
                is_negotiable=is_negotiable,
                is_active=True,
                created_by=Company.objects.get(created_by=request.user),
                property_type='shp'
            )
            my_property.save()
            serializer = PropertySerializer(my_property, context={'request': request})
            return Response({'results': serializer.data, 'response': 'Success'}, status=HTTP_201_CREATED)
        else:
            return Response({'detail': 'Setup an Company profile before enlisting your properties',
                             'response': 'Failure'}, status=HTTP_400_BAD_REQUEST)


class PropertyDetail(RetrieveAPIView):
    permission_classes = (AllowAny,)
    queryset = Property.objects.filter(is_active=True)
    serializer_class = PropertySerializer
    lookup_field = 'id'

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
