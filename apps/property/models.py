from apps.company.models import *
from django.db import models
from django.utils import timezone

PROPERTY_TYPE = {
    ('land', 'Land'),
    ('apart', 'Apartment'),
    ('off', 'Office'),
    ('hse', 'Houses'),
    ('shp', 'Shops')

}

LOCATIONS = {
    ('Victoriaborg', 'Victoriaborg'),
    ('East Ridge', 'East Ridge'),
    ('West Ridge', 'West Ridge'),
    ('North Ridge', 'North Ridge'),
    ('Adabraka', 'Adabraka'),
    ('Asylum Down', 'Asylum Down'),
    ('McCarthy Hill', 'McCarthy Hill'),
    ('Airport Residential Area', 'Airport Residential Area'),
    ('Roman Ridge', 'Roman Ridge'),
    ('Kanda', 'Kanda'),
    ('Dzorwulu', 'Dzorwulu'),
    ('East Legon', 'East Legon'),
    ('Kaneshie', 'Kaneshie'),
    ('Kokomlemle', 'Kokomlemle'),
    ('Tesano', 'Tesano'),
    ('West Legon', 'West Legon'),
    ('Abelemkpe', 'Abelemkpe'),
    ('Cantonments', 'Cantonments'),
    ('Labone', 'Labone'),
    ('Airport Hills', 'Airport Hills'),
    ('Burma Camp', 'Burma Camp'),
    ('Chorkor', 'Chorkor'),
    ('Mamprobi', 'Mamprobi'),
    ('Korle Bu', 'Korle Bu'),
    ('Korle Gonno', 'Korle Gonno'),
    ('Lartebiokorshie', 'Lartebiokorshie'),
    ('Mataheko', 'Mataheko'),
    ('Dansoman', 'Dansoman'),
    ('Adenta', 'Adenta')
}


RENT_DURATION = {
    ('pm', 'Per Month'),
    ('py', 'Per Year'),
    ('pd', 'Per Day'),
    ('pw', 'Per Week'),
}


RENT_ADVANCE = {
    (1, 'One Month'),
    (2, 'Six Month'),
    (3, 'One Year'),
    (4, 'Two Years'),
}


LAND_TYPE = {
    ('agric', 'Agriculture'),
    ('comm', 'Commercial'),
    ('res', 'Residential'),
    ('oth', 'Other')
}

UNIT = {
    ('acre', 'acre'),
    ('sqrt', 'sqrt'),
    ('m', 'm2')
}


class Property(models.Model):
    property_type = models.CharField(choices=PROPERTY_TYPE, max_length=255, blank=True, null=True)
    location = models.CharField(choices=LOCATIONS, max_length=255, blank=True, null=True)
    description = models.TextField(max_length=300, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    is_broker = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    is_furnished = models.BooleanField(default=False)
    has_washroom = models.BooleanField(default=False)
    is_compound = models.BooleanField(default=False)
    size = models.CharField(max_length=255, blank=True, null=True)
    is_negotiable = models.BooleanField(default=False)
    is_for_sale = models.BooleanField(default=False)
    is_sold = models.BooleanField(default=False)
    is_for_rent = models.BooleanField(default=False)
    is_rented = models.BooleanField(default=False)
    rent_duration = models.CharField(choices=RENT_DURATION, max_length=255, blank=True, null=True)
    rent_advance = models.IntegerField(choices=RENT_ADVANCE, blank=True, null=True)
    land_type = models.CharField(choices=LAND_TYPE, max_length=255, blank=True, null=True)
    unit = models.CharField(choices=UNIT, max_length=255, blank=True, null=True)
    landmark = models.CharField(max_length=255, blank=True, null=True)
    bedrooms = models.IntegerField(blank=True, null=True)
    washroom = models.IntegerField(blank=True, null=True)
    photo = models.FileField(blank=True, null=True, upload_to='')
    created_by = models.ForeignKey(
        Company,
        related_name='properties',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    def __str__(self):
        return f'{self.title}, {self.get_location_display()} - {self.price}'

    class Meta:
        ordering = ('created_at', )
        verbose_name_plural = 'Properties'
