from django.urls import path
from .views import *

app_name = 'property'

urlpatterns = [
    path('list/', ListProperties.as_view(), name='list'),
    path('land/list/', ListLands.as_view(), name='land-list'),
    path('land/rent/list/', ListLandForRent.as_view(), name='land-rent-list'),
    path('land/sale/list', ListLandForSale.as_view(), name='land-sale-list'),
    path('apartment/list/', ListApartments.as_view(), name='apartment-list'),
    path('apartment/rent/list/', ListApartmentsForRent.as_view(), name='apartment-rent-list'),
    path('apartment/sale/list', ListApartmentForSale.as_view(), name='apartment-sale-list'),
    path('office/list/', ListOffices.as_view(), name='office-list'),
    path('office/rent/list/', ListOfficeForRent.as_view(), name='office-rent-list'),
    path('office/sale/list', ListOfficeForSale.as_view(), name='office-sale-list'),
    path('shop/list/', ListShops.as_view(), name='shop-list'),
    path('shop/rent/list/', ListShopForRent.as_view(), name='shop-rent-list'),
    path('shop/sale/list', ListShopForSale.as_view(), name='shop-sale-list'),
    path('house/list/', ListHouses.as_view(), name='house-list'),
    path('house/rent/list/', ListHouseForRent.as_view(), name='house-rent-list'),
    path('house/sale/list', ListHouseForSale.as_view(), name='house-sale-list'),
    path('search/', SearchProperty.as_view(), name='search'),
    path('filter/', FilterProperties.as_view(), name='filter'),
    path('land/create/', CreateLand.as_view(), name='land-create'),
    path('apartment/create/', CreateApartment.as_view(), name='apartment-create'),
    path('office/create/', CreateOffice.as_view(), name='office-create'),
    path('shop/create/', CreateShops.as_view(), name='shop-create'),
    path('house/create/', CreateHouses.as_view(), name='house-create'),
    path('company/properties/', ListCompanyProperties.as_view(), name='company-properties'),
    path('detail/<id>/', PropertyDetail.as_view(), name='detail'),
]