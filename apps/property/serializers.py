from rest_framework.serializers import *
from . models import *


class PropertySerializer(ModelSerializer):
    location_display = SerializerMethodField()
    property_type_display = SerializerMethodField()
    rent_duration_display = SerializerMethodField()
    rent_advance_display = SerializerMethodField()
    land_type_display = SerializerMethodField()
    created_by = ReadOnlyField(source='created_by.business_name')

    class Meta:
        model = Property
        fields = ('id', 'property_type', 'property_type_display', 'location', 'location_display', 'description',
                  'title', 'is_broker', 'is_active', 'price', 'is_furnished', 'has_washroom', 'is_compound', 'size',
                  'is_negotiable', 'is_for_sale', 'is_sold', 'is_for_rent', 'is_rented', 'rent_duration',
                  'rent_duration_display', 'rent_advance', 'rent_advance_display', 'land_type', 'land_type_display',
                  'unit', 'landmark', 'bedrooms', 'washroom', 'photo', 'created_at', 'created_by')
        read_only_fields = ('id', 'created_at', 'created_by')

    def get_location_display(self, obj):
        return obj.get_location_display()

    def get_property_type_display(self, obj):
        return obj.get_property_type_display()

    def get_rent_duration_display(self, obj):
        return obj.get_rent_duration_display()

    def get_rent_advance_display(self, obj):
        return obj.get_rent_advance_display()

    def get_land_type_display(self, obj):
        return obj.get_land_type_display()
