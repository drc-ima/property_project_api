"""property_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from apps.users import urls as user_urls
from apps.company import urls as company_urls
from apps.property import urls as property_urls
# from apps.property_web import urls as property_web_urls

api_patterns = [
    path('users/', include(user_urls, namespace='users')),
    path('company/', include(company_urls, namespace='company')),
    path('property/', include(property_urls, namespace='property')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/token/', obtain_jwt_token),
    path('auth/refresh_token/', refresh_jwt_token),
    path('api/', include(api_patterns), name='api'),
    path('', TemplateView.as_view(template_name='index.html'), name='home'),
    # path('property/', include(property_web_urls, namespace='web-property')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
